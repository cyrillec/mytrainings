
class IntegerType:
  def __init__(self):
    pass
  def dump(self):
    return "Integer"

class CharType:
  def __init__(self):
    pass
  def dump(self):
    return "Char"

class VoidType:
  def __init__(self):
    pass
  def dump(self):
    return "Void"

class Char:
  def __init__(self, c):
    self.value = c
  def dump(self):
    return "\'%c\'"%self.value

class Integer:
  def __init__(self, n):
    self.value = n
  def dump(self):
    return self.value

class Identifier:
  def __init__(self, name):
    self.name = name
  def dump(self):
    return "Identifier(name=%s)"%self.name

class Function:
  def __init__(self, t, name, args, body):
    self.type = t
    self.identifier = name
    self.args = args
    self.body = body
  def dump(self):
    return "Function(identifier=%s, type=%s, args=%s, body=%s)"%(self.identifier.dump(), self.type.dump(), [x.dump() for x in self.args], [x.dump() for x in self.body])

class Call:
  def __init__(self, identifier, args):
    self.identifier = identifier
    self.args = args
  def dump(self):
    return "Call(identifier=%s, args=%s)"%(self.identifier.dump(), [x.dump() for x in self.args])

class Declaration:
  def __init__(self, t, name):
    self.type = t
    self.identifier = name
  def dump(self):
    return "Declaration(identifier=%s, type=%s)"%(self.identifier.dump(), self.type.dump())

class Assignment:
  def __init__(self, left, oper, right):
    self.left = left
    self.oper = oper
    self.right = right
  def dump(self):
    return "Assignement(left=%s, oper=\"%s\", right=%s)"%(self.left.dump(), self.oper, self.right.dump())

class Operation:
  def __init__(self, left, oper, right):
    self.left = left
    self.oper = oper
    self.right = right
  def dump(self):
    return "Operation(left=%s, oper=\"%s\", right=%s)"%(self.left.dump(), self.oper, self.right.dump())

class Condition:
  def __init__(self, left, oper, right):
    self.left = left
    self.oper = oper
    self.right = right
  def dump(self):
    return "Condition(left=%s, oper=\"%s\", right=%s)"%(self.left.dump(), self.oper, self.right())

class If:
  def __init__(self, condition, body=[], orelse=[]):
    self.condition = condition 
    self.body = body 
    self.orelse = orelse
  def dump(self):
    return "If(condition=%s, body=%s, orelse=%s)"%(self.condition.dump(), [x.dump() for x in self.body], [x.dump() for x in self.orelse])

class While:
  def __init__(self, condition, body=[]):
    self.condition = conditon 
    self.body = body

class Return:
  def __init__(self, value):
    self.value = value
  def dump(self):
    return "Return(%s)"%self.value.dump()

class Program:
  def __init__(self, body):
    self.body = body
  def dump(self):
    return "Program(body=%s)"%[x.dump() for x in self.body]

