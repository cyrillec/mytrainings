import string

class Lexer:
  def __init__(self, code):
    self.code =  [x for x in code]
    self.tokens = []

  def getChar(self):
    if len(self.code) > 0:
      return self.code.pop(0)
    else:
      return None

  def resetCurrentToken(self):
    self.current_token = ""

  def pushToken(self, token):
    self.tokens.append(token)

  def commitToken(self):
    if self.current_token:
      self.pushToken(self.current_token)
      self.resetCurrentToken()

  def getTokens(self):
    c = self.getChar()
    self.resetCurrentToken()
    while c:
      if c in ["{", "}", "(", ")", ";", "=", ">", "<", "!", "+", "-", "/", "*", ","]:
        self.commitToken()
        self.pushToken(c)
      elif c in string.ascii_letters or c in string.digits:
        self.current_token += c
      elif c in string.whitespace or c in "\r\n":
        self.commitToken()
      c = self.getChar()

  def mergeToken(self, i, j):
    tokens = self.tokens[:i]
    token = "".join(self.tokens[i:j+1])
    tokens.append(token)
    tokens += self.tokens[j+1:]
    self.tokens = tokens

  def optimizeTokens(self, i):
    if self.tokens[i] in ["=", ">", "<", "!"]:
      if self.tokens[i+1] in ["="]:
       self.mergeToken(i, i+1)
    if self.tokens[i] in ["+", "-", "*"]:
      if self.tokens[i+1] in ["="]:
       self.mergeToken(i, i+1)
      elif self.tokens[i] in ["+", "-"] and self.tokens[i+1] == self.tokens[i]:
       self.mergeToken(i, i+1)
    if i < len(self.tokens)-1:
      self.optimizeTokens(i+1)
    
  def Tokenize(self):
    self.getTokens()
    self.optimizeTokens(0)
