from statements import *
import string


"""
     type:identifier:arguments:body := function
     type:identifier := declaration
     identifier:oper:identifier|integer|operation := assignement
     identifier:oper:identifier|integer|operation := operation
     identifier:arguments := call
     'if':condition:body := if
     'return':identifier|integer := return
"""

class ParserError(Exception):
  pass

class ASTParser:
  def __init__(self, tokens):
    self.tokens = tokens
    self.shiftToken()

  def tryParse(self, function):
    tokens = self.tokens[:]
    current_token = self.current_token
    next_token = self.next_token
    try:
      res = function()
    except ParserError:
      self.current_token = current_token
      self.next_token = next_token
      self.tokens = tokens
      return None
    return res

  def tryParseFrom(self, functions):
    for f in functions:
      x = self.tryParse(f)
      if x:
        return x

  def shiftToken(self):
    if len(self.tokens) > 0:
      self.current_token = self.tokens.pop(0)
      if self.tokens:
        self.next_token = self.tokens[0]
      else:
        self.next_token = None
      return self.current_token
    self.current_token = None
    return None

  def ParseType(self):
    if self.current_token == 'char':
      self.shiftToken()
      return CharType()
    elif self.current_token == 'int':
      self.shiftToken()
      return IntegerType()
    elif self.current_token == 'void':
      self.shiftToken()
      return VoidType()
    raise ParserError("Expecting type")

  def ParseIdentifier(self):
    i = self.current_token
    if not i or i[0] not in string.ascii_letters:
      raise ParserError("identifier should start whith alphabet char")
    self.shiftToken()
    return Identifier( i )

  def ParseInteger(self):
    n = self.current_token
    try:
      n = int(n)
    except:
      raise ParserError("not a valid number")
    self.shiftToken()
    return Integer(n)

  def ParseArgsDec(self):
    def appendArg(s):
      t = s.ParseType()
      i = s.ParseIdentifier()
      args.append( Declaration(t, i) )
    self.shiftToken()
    args = []
    if self.current_token == ')':
      self.shiftToken()
      return args
    appendArg(self) 
    while self.current_token == ',':
      self.shiftToken()
      appendArg(self) 
    if self.current_token != ')':
      raise ParserError("Expecting ')' character for argument parsing") 
    self.shiftToken()
    return args

  def ParseArgsCall(self):
    def appendArg(s):
      arg =  self.tryParseFrom([self.ParseIdentifier,self.ParseInteger])
      args.append( arg )
    self.shiftToken()
    args = []
    if self.current_token == ')':
      self.shiftToken()
      return args
    appendArg(self)
    while self.current_token == ',':
      self.shiftToken()
      appendArg(self)
    if self.current_token != ')':
      raise ParserError("Expecting ')' character for argument parsing")
    self.shiftToken()
    return args
     
  def ParseOperation(self):
    left = None
    right = None
    left = self.tryParseFrom([self.ParseIdentifier, self.ParseInteger])
    if not left:
      raise ParserError("Invalid left operand")
    if self.current_token not in ["+", "-", "*", "/"]:
      raise ParserError("Invalid operator")
    op = self.current_token
    self.shiftToken()
    right = self.tryParseFrom([self.ParseOperation, self.ParseIdentifier, self.ParseInteger])
    if not right:
      raise ParserError("Invalid right operand")
    return Operation(left, op, right)

  def ParseAssignment(self):
    right = None
    i = self.ParseIdentifier()
    if self.current_token not in ["=", "+=", "-=", "*=" ]:
      raise ParserError("Invalid operator for assignement")
    op = self.current_token
    self.shiftToken()
    right = self.tryParseFrom([self.ParseOperation,
                               self.ParseIdentifier,
                               self.ParseCall,
                               self.ParseInteger])
    if not right:
      raise ParserError("Invalid assignement")
    return Assignment(i, op, right)

  def ParseCondition(self):
    left = None
    right = None
    if self.current_token != '(':
      raise ParserError("Invalid condition")
    self.shiftToken()
    left = self.tryParseFrom([self.ParseIdentifier, self.ParseInteger])
    if not left:
      raise ParserError("Invalid condition left operant")
    if self.current_token not in ["==", ">=", "<=", "!="]:
      raise ParserError("Invalid condition operator") 
    oper = self.current_token
    self.shiftToken()
    right = self.tryParseFrom([self.ParseIdentifier, self.ParseInteger])
    if not right:
      raise ParserError("Invalid condition right operant")
    if self.current_token != ')':
      raise ParserError("Invalid condition")
    self.shiftToken()
    return Condition(left, oper, right)

  def ParseIf(self):
    if self.current_token != "if":
      raise ParserError("Expecting 'if' for if statement")
    self.shiftToken()
    cond = self.ParseCondition()
    body = self.ParseBody()
    if self.current_token != "else":
      return If(cond, body)
    self.shiftToken()
    elsebody = self.ParseBody()
    return If(cond, body, elsebody)
    
  def ParseExpression(self):
    exp = self.tryParseFrom([self.ParseDeclaration,
                             self.ParseAssignment,
                             self.ParseCall,
                             self.ParseReturn])
    if not exp:
      return None
    if self.current_token != ";":
      raise ParserError("Expecting ';' ending assignement")
    self.shiftToken()
    return exp

  def ParseCall(self):
    identifier = self.ParseIdentifier()
    if self.current_token != '(':
      raise ParserError("Expecting '(' for function call")
    args = self.ParseArgsCall()
    return Call(identifier, args)

  def ParseReturn(self):
    if self.current_token != 'return':
      raise ParserError("Exepecting 'return' keyword")
    self.shiftToken()
    r = self.tryParseFrom([self.ParseInteger, self.ParseIdentifier])
    if not r:
      raise ParserError("No valid return value")
    return Return(r)

  def ParseBody(self):
    self.shiftToken()
    body = []
    if self.current_token == '}':
      self.shiftToken()
      return body
    exp = self.ParseExpression()
    while exp:
      body.append( exp )
      exp = self.ParseExpression()
    if self.current_token == '}':
      self.shiftToken()
      return body
    raise ParserError("Expecting '}' for ending body")

  def ParseDeclaration(self):
    t = self.ParseType()
    i = self.ParseIdentifier()
    return Declaration(t, i)

  def ParseDeclarationFunction(self):
    t = self.ParseType()
    i = self.ParseIdentifier()
    if self.current_token == '(':
      args = self.ParseArgsDec()
      if self.current_token == '{':
        body = self.ParseBody()
        return Function(t, i, args, body)
      elif self.current_token == ';':
        return Function(t, i, args, [])
      else:
        raise ParserError("Expecting ';' or '{' for function declaration")
    raise ParserError("Invalid declaration")

  def ParseProgram(self):
    #TODO: Progam can handle definitions, imports, etc.
    body = []
    declaration = self.tryParseFrom([self.ParseDeclarationFunction, self.ParseDeclaration])
    while declaration:
      body.append(declaration)
      if self.current_token==";":
        self.shiftToken()
      declaration = self.tryParseFrom([self.ParseDeclarationFunction, self.ParseDeclaration])
    return Program(body)

