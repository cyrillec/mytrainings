from ast_parser import *
from statements import *

def test_declaration():
  parse = ASTParser(["int", "test", ";"])
  dec = parse.ParseDeclaration()
  assert( type(dec) == Declaration )
  assert( type(dec.type) == IntegerType )
  assert( type(dec.identifier) == Identifier )
  assert( dec.identifier.name == "test")

def test_declaration_func():
  parse = ASTParser(["char", "test", "(", "int", "i", ",", "char", "a", ")","{", "}"])
  dec = parse.ParseDeclarationFunction()
  assert( type(dec) == Function )
  assert( type(dec.type) == CharType )
  assert( type(dec.identifier) == Identifier )
  assert( dec.identifier.name == "test")
  assert( len(dec.args) == 2 )
  assert( type(dec.args[0].type) == IntegerType )
  assert( dec.args[0].identifier.name == "i" )
  assert( type(dec.args[1].type) == CharType )
  assert( dec.args[1].identifier.name == "a" )


def test_assignement():
  parse = ASTParser(["i", "=", "j", "+", "3", ";"])
  assign = parse.ParseAssignment()
  assert( type(assign) == Assignment )
  assert( type(assign.left) == Identifier )
  assert( assign.left.name == "i" )
  assert( assign.oper == "=" )
  assert( type(assign.right) == Operation )
  assert( type(assign.right.left) == Identifier )
  assert( assign.right.left.name == "j" )
  assert( type(assign.right.right) == Integer )
  assert( assign.right.right.value == 3 )
  assert( assign.right.oper == "+" )


def test_if():
  parse = ASTParser(["if", "(", "i", "==", "3", ")", "{", "}"])
  s = parse.ParseIf()
  assert( type(s) == If )
  assert( s.condition.left.name == "i" )
  assert( s.condition.oper == "==" )
  assert( s.condition.right.value == 3 )

def test_call():
  parse = ASTParser(["test", "(", "1", ",", "i", ")"])
  c = parse.ParseCall()
  assert( type(c) == Call)
  assert( c.identifier.name == "test")
  assert( c.args[0].value == 1)
  assert( c.args[1].name == "i")

def test_body():
  parse = ASTParser(["{", "int", "i", ";", "i", "+=", "1", ";", "}"])
  body = parse.ParseBody()
  assert( type(body) == list )
  assert( type(body[0]) == Declaration )
  assert( type(body[1]) == Assignment )
