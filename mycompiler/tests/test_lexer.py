import lexer

def test_lexer():
  code = """
void main (){
  int i = 1 ;
  i += i + 2;
  return i++;
}
"""
  l=lexer.Lexer(code)
  l.Tokenize()
  assert(l.tokens == ['void', 'main', '(', ')', '{', 'int', 'i', '=', '1', ';', 'i', '+=', 'i', '+', '2', ';', 'return', 'i', '++', ';', '}'])

