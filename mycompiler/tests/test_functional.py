import lexer
import ast_parser

def test_functional():
  code = """
int i;
int main(){
  int j;
  i = 1;
  j = i + j;
  j = j + 3;
  return j;
}
"""
  l = lexer.Lexer(code)
  l.Tokenize()
  p = ast_parser.ASTParser( l.tokens )
  ast = p.ParseProgram()
  ast.dump()

