import lexer
import ast_parser
code = """
int i;
int main(){
  int j;
  i = 1;
  j = i + j;
  j = j + 3;
  return j;
}
"""
l = lexer.Lexer(code)
l.Tokenize()
p = ast_parser.ASTParser( l.tokens )
prog = p.ParseProgram()
print(prog.dump())

